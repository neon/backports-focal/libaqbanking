/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "cryptparams.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifndef AQFINTS_CRYPTPARAMS_CRYPTPARAMS_P_H
#define AQFINTS_CRYPTPARAMS_CRYPTPARAMS_P_H

#include "./cryptparams.h"


#ifdef __cplusplus
extern "C" {
#endif

struct AQFINTS_CRYPTPARAMS {
  char *securityProfileName;
  int securityProfileVersion;
  AQFINTS_CRYPTPARAMS_SIGNALGO signAlgo;
  AQFINTS_CRYPTPARAMS_OPMODE opModeSign;
  AQFINTS_CRYPTPARAMS_OPMODE opModeAuth;
  AQFINTS_CRYPTPARAMS_SIGUSAGE sigUsage;
  AQFINTS_CRYPTPARAMS_HASHALGO hashAlgoSign;
  AQFINTS_CRYPTPARAMS_HASHALGO hashAlgoAuth;
  AQFINTS_CRYPTPARAMS_CRYPTALGO cryptAlgo;
  AQFINTS_CRYPTPARAMS_OPMODE opModeCrypt;
};

#ifdef __cplusplus
}
#endif

#endif

