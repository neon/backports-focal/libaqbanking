/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "bpdaddrservice.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifndef AQFINTS_BPDADDR_SERVICE_BPDADDRSERVICE_H
#define AQFINTS_BPDADDR_SERVICE_BPDADDRSERVICE_H


#ifdef __cplusplus
extern "C" {
#endif

/** @page P_AQFINTS_BPDADDR_SERVICE Structure AQFINTS_BPDADDR_SERVICE
<p>This page describes the properties of AQFINTS_BPDADDR_SERVICE.</p>



<h1>AQFINTS_BPDADDR_SERVICE</h1>



@anchor AQFINTS_BPDADDR_SERVICE_flags
<h2>flags</h2>

<p>Set this property with @ref AQFINTS_BpdAddrService_SetFlags(), get it with @ref AQFINTS_BpdAddrService_GetFlags().</p>


@anchor AQFINTS_BPDADDR_SERVICE_type
<h2>type</h2>

<p>Set this property with @ref AQFINTS_BpdAddrService_SetType(), get it with @ref AQFINTS_BpdAddrService_GetType().</p>


@anchor AQFINTS_BPDADDR_SERVICE_address
<h2>address</h2>

<p>Set this property with @ref AQFINTS_BpdAddrService_SetAddress(), get it with @ref AQFINTS_BpdAddrService_GetAddress().</p>


@anchor AQFINTS_BPDADDR_SERVICE_suffix
<h2>suffix</h2>

<p>Set this property with @ref AQFINTS_BpdAddrService_SetSuffix(), get it with @ref AQFINTS_BpdAddrService_GetSuffix().</p>


@anchor AQFINTS_BPDADDR_SERVICE_filter
<h2>filter</h2>

<p>Set this property with @ref AQFINTS_BpdAddrService_SetFilter(), get it with @ref AQFINTS_BpdAddrService_GetFilter().</p>


@anchor AQFINTS_BPDADDR_SERVICE_filterVersion
<h2>filterVersion</h2>

<p>Set this property with @ref AQFINTS_BpdAddrService_SetFilterVersion(), get it with @ref AQFINTS_BpdAddrService_GetFilterVersion().</p>


@anchor AQFINTS_BPDADDR_SERVICE_runtimeFlags
<h2>runtimeFlags</h2>

<p>Set this property with @ref AQFINTS_BpdAddrService_SetRuntimeFlags(), get it with @ref AQFINTS_BpdAddrService_GetRuntimeFlags().</p>

*/

/* define AQFINTS_BPDADDR_SERVICE_RTFLAGS */


/* define AQFINTS_BPDADDR_SERVICE_FLAGS */


/* needed system headers */
#include <gwenhywfar/types.h>
#include <gwenhywfar/list1.h>
#include <gwenhywfar/db.h>

/* pre-headers */
#include <libaqfints/aqfints.h>

typedef struct AQFINTS_BPDADDR_SERVICE AQFINTS_BPDADDR_SERVICE;
GWEN_LIST_FUNCTION_DEFS(AQFINTS_BPDADDR_SERVICE, AQFINTS_BpdAddrService)



/* post-headers */


/** Constructor. */
AQFINTS_BPDADDR_SERVICE *AQFINTS_BpdAddrService_new(void);

/** Destructor. */
void AQFINTS_BpdAddrService_free(AQFINTS_BPDADDR_SERVICE *p_struct);

void AQFINTS_BpdAddrService_Attach(AQFINTS_BPDADDR_SERVICE *p_struct);

AQFINTS_BPDADDR_SERVICE *AQFINTS_BpdAddrService_dup(const AQFINTS_BPDADDR_SERVICE *p_src);

AQFINTS_BPDADDR_SERVICE *AQFINTS_BpdAddrService_copy(AQFINTS_BPDADDR_SERVICE *p_struct, const AQFINTS_BPDADDR_SERVICE *p_src);

/** Getter.
 * Use this function to get the member "flags" (see @ref AQFINTS_BPDADDR_SERVICE_flags)
*/
uint32_t AQFINTS_BpdAddrService_GetFlags(const AQFINTS_BPDADDR_SERVICE *p_struct);

/** Getter.
 * Use this function to get the member "type" (see @ref AQFINTS_BPDADDR_SERVICE_type)
*/
int AQFINTS_BpdAddrService_GetType(const AQFINTS_BPDADDR_SERVICE *p_struct);

/** Getter.
 * Use this function to get the member "address" (see @ref AQFINTS_BPDADDR_SERVICE_address)
*/
const char *AQFINTS_BpdAddrService_GetAddress(const AQFINTS_BPDADDR_SERVICE *p_struct);

/** Getter.
 * Use this function to get the member "suffix" (see @ref AQFINTS_BPDADDR_SERVICE_suffix)
*/
const char *AQFINTS_BpdAddrService_GetSuffix(const AQFINTS_BPDADDR_SERVICE *p_struct);

/** Getter.
 * Use this function to get the member "filter" (see @ref AQFINTS_BPDADDR_SERVICE_filter)
*/
const char *AQFINTS_BpdAddrService_GetFilter(const AQFINTS_BPDADDR_SERVICE *p_struct);

/** Getter.
 * Use this function to get the member "filterVersion" (see @ref AQFINTS_BPDADDR_SERVICE_filterVersion)
*/
int AQFINTS_BpdAddrService_GetFilterVersion(const AQFINTS_BPDADDR_SERVICE *p_struct);

/** Getter.
 * Use this function to get the member "runtimeFlags" (see @ref AQFINTS_BPDADDR_SERVICE_runtimeFlags)
*/
uint32_t AQFINTS_BpdAddrService_GetRuntimeFlags(const AQFINTS_BPDADDR_SERVICE *p_struct);

/** Setter.
 * Use this function to set the member "flags" (see @ref AQFINTS_BPDADDR_SERVICE_flags)
*/
void AQFINTS_BpdAddrService_SetFlags(AQFINTS_BPDADDR_SERVICE *p_struct, uint32_t p_src);

/** Add flags.
 * Use this function to add flags to member "flags" (see @ref AQFINTS_BPDADDR_SERVICE_flags)
*/
void AQFINTS_BpdAddrService_AddFlags(AQFINTS_BPDADDR_SERVICE *p_struct, uint32_t p_src);
/** Add flags.
 * Use this function to add flags to member "flags" (see @ref AQFINTS_BPDADDR_SERVICE_flags)
*/
void AQFINTS_BpdAddrService_SubFlags(AQFINTS_BPDADDR_SERVICE *p_struct, uint32_t p_src);

/** Setter.
 * Use this function to set the member "type" (see @ref AQFINTS_BPDADDR_SERVICE_type)
*/
void AQFINTS_BpdAddrService_SetType(AQFINTS_BPDADDR_SERVICE *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "address" (see @ref AQFINTS_BPDADDR_SERVICE_address)
*/
void AQFINTS_BpdAddrService_SetAddress(AQFINTS_BPDADDR_SERVICE *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "suffix" (see @ref AQFINTS_BPDADDR_SERVICE_suffix)
*/
void AQFINTS_BpdAddrService_SetSuffix(AQFINTS_BPDADDR_SERVICE *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "filter" (see @ref AQFINTS_BPDADDR_SERVICE_filter)
*/
void AQFINTS_BpdAddrService_SetFilter(AQFINTS_BPDADDR_SERVICE *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "filterVersion" (see @ref AQFINTS_BPDADDR_SERVICE_filterVersion)
*/
void AQFINTS_BpdAddrService_SetFilterVersion(AQFINTS_BPDADDR_SERVICE *p_struct, int p_src);

/** Setter.
 * Use this function to set the member "runtimeFlags" (see @ref AQFINTS_BPDADDR_SERVICE_runtimeFlags)
*/
void AQFINTS_BpdAddrService_SetRuntimeFlags(AQFINTS_BPDADDR_SERVICE *p_struct, uint32_t p_src);

/** Add flags.
 * Use this function to add flags to member "runtimeFlags" (see @ref AQFINTS_BPDADDR_SERVICE_runtimeFlags)
*/
void AQFINTS_BpdAddrService_AddRuntimeFlags(AQFINTS_BPDADDR_SERVICE *p_struct, uint32_t p_src);
/** Add flags.
 * Use this function to add flags to member "runtimeFlags" (see @ref AQFINTS_BPDADDR_SERVICE_runtimeFlags)
*/
void AQFINTS_BpdAddrService_SubRuntimeFlags(AQFINTS_BPDADDR_SERVICE *p_struct, uint32_t p_src);

AQFINTS_BPDADDR_SERVICE_LIST *AQFINTS_BpdAddrService_List_dup(const AQFINTS_BPDADDR_SERVICE_LIST *p_src);

void AQFINTS_BpdAddrService_ReadDb(AQFINTS_BPDADDR_SERVICE *p_struct, GWEN_DB_NODE *p_db);

int AQFINTS_BpdAddrService_WriteDb(const AQFINTS_BPDADDR_SERVICE *p_struct, GWEN_DB_NODE *p_db);

AQFINTS_BPDADDR_SERVICE *AQFINTS_BpdAddrService_fromDb(GWEN_DB_NODE *p_db);

int AQFINTS_BpdAddrService_toDb(const AQFINTS_BPDADDR_SERVICE *p_struct, GWEN_DB_NODE *p_db);

/* end-headers */


#ifdef __cplusplus
}
#endif

#endif

