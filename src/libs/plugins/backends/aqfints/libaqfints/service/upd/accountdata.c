/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "accountdata.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "./accountdata_p.h"

#include <gwenhywfar/misc.h>
#include <gwenhywfar/debug.h>

/* code headers */
#include <string.h>

/* macro functions */
GWEN_LIST_FUNCTIONS(AQFINTS_ACCOUNTDATA, AQFINTS_AccountData)


AQFINTS_ACCOUNTDATA *AQFINTS_AccountData_new(void) {
  AQFINTS_ACCOUNTDATA *p_struct;

  GWEN_NEW_OBJECT(AQFINTS_ACCOUNTDATA, p_struct)
  p_struct->_refCount=1;
  GWEN_LIST_INIT(AQFINTS_ACCOUNTDATA, p_struct)
  /* members */
  p_struct->accountNumber=NULL;
  p_struct->accountSuffix=NULL;
  p_struct->bankCode=NULL;
  p_struct->country=0;
  p_struct->iban=NULL;
  p_struct->accountName=NULL;
  p_struct->customerId=NULL;
  p_struct->accountType=0;
  p_struct->currency=NULL;
  p_struct->name1=NULL;
  p_struct->name2=NULL;
  p_struct->limitType=0;
  p_struct->limitValue=AB_Value_new();
  p_struct->limitDays=0;
  p_struct->updJobs=NULL;
  p_struct->genericExtension=NULL;
  p_struct->runtimeFlags=0;
  /* virtual functions */

  return p_struct;
}

void AQFINTS_AccountData_free(AQFINTS_ACCOUNTDATA *p_struct) {
  if (p_struct) {
  assert(p_struct->_refCount);
  if (p_struct->_refCount==1) {
    GWEN_LIST_FINI(AQFINTS_ACCOUNTDATA, p_struct)
  /* members */
    free(p_struct->accountNumber); p_struct->accountNumber=NULL;
    free(p_struct->accountSuffix); p_struct->accountSuffix=NULL;
    free(p_struct->bankCode); p_struct->bankCode=NULL;
    free(p_struct->iban); p_struct->iban=NULL;
    free(p_struct->accountName); p_struct->accountName=NULL;
    free(p_struct->customerId); p_struct->customerId=NULL;
    free(p_struct->currency); p_struct->currency=NULL;
    free(p_struct->name1); p_struct->name1=NULL;
    free(p_struct->name2); p_struct->name2=NULL;
    AB_Value_free(p_struct->limitValue);
    AQFINTS_UpdJob_List_free(p_struct->updJobs); p_struct->updJobs=NULL;
    free(p_struct->genericExtension); p_struct->genericExtension=NULL;
    p_struct->_refCount=0;
    GWEN_FREE_OBJECT(p_struct);
  }
  else
    p_struct->_refCount--;
  }
}

void AQFINTS_AccountData_Attach(AQFINTS_ACCOUNTDATA *p_struct) {
  assert(p_struct);
  assert(p_struct->_refCount);
  p_struct->_refCount++;
}

AQFINTS_ACCOUNTDATA *AQFINTS_AccountData_dup(const AQFINTS_ACCOUNTDATA *p_src) {
  AQFINTS_ACCOUNTDATA *p_struct;

  assert(p_src);
  p_struct=AQFINTS_AccountData_new();
  /* member "accountNumber" */
  if (p_struct->accountNumber) {
    free(p_struct->accountNumber); p_struct->accountNumber=NULL;
  }
  if (p_src->accountNumber) {
    p_struct->accountNumber=strdup(p_src->accountNumber);
  }


  /* member "accountSuffix" */
  if (p_struct->accountSuffix) {
    free(p_struct->accountSuffix); p_struct->accountSuffix=NULL;
  }
  if (p_src->accountSuffix) {
    p_struct->accountSuffix=strdup(p_src->accountSuffix);
  }


  /* member "bankCode" */
  if (p_struct->bankCode) {
    free(p_struct->bankCode); p_struct->bankCode=NULL;
  }
  if (p_src->bankCode) {
    p_struct->bankCode=strdup(p_src->bankCode);
  }


  /* member "country" */
    p_struct->country=p_src->country;


  /* member "iban" */
  if (p_struct->iban) {
    free(p_struct->iban); p_struct->iban=NULL;
  }
  if (p_src->iban) {
    p_struct->iban=strdup(p_src->iban);
  }


  /* member "accountName" */
  if (p_struct->accountName) {
    free(p_struct->accountName); p_struct->accountName=NULL;
  }
  if (p_src->accountName) {
    p_struct->accountName=strdup(p_src->accountName);
  }


  /* member "customerId" */
  if (p_struct->customerId) {
    free(p_struct->customerId); p_struct->customerId=NULL;
  }
  if (p_src->customerId) {
    p_struct->customerId=strdup(p_src->customerId);
  }


  /* member "accountType" */
    p_struct->accountType=p_src->accountType;


  /* member "currency" */
  if (p_struct->currency) {
    free(p_struct->currency); p_struct->currency=NULL;
  }
  if (p_src->currency) {
    p_struct->currency=strdup(p_src->currency);
  }


  /* member "name1" */
  if (p_struct->name1) {
    free(p_struct->name1); p_struct->name1=NULL;
  }
  if (p_src->name1) {
    p_struct->name1=strdup(p_src->name1);
  }


  /* member "name2" */
  if (p_struct->name2) {
    free(p_struct->name2); p_struct->name2=NULL;
  }
  if (p_src->name2) {
    p_struct->name2=strdup(p_src->name2);
  }


  /* member "limitType" */
    p_struct->limitType=p_src->limitType;


  /* member "limitValue" */
  if (p_struct->limitValue) {
    AB_Value_free(p_struct->limitValue);
  }
  if (p_src->limitValue) {
    p_struct->limitValue=AB_Value_dup(p_src->limitValue);
  }


  /* member "limitDays" */
    p_struct->limitDays=p_src->limitDays;


  /* member "updJobs" */
  if (p_struct->updJobs) {
    AQFINTS_UpdJob_List_free(p_struct->updJobs); p_struct->updJobs=NULL;
  }
  if (p_src->updJobs) {
    p_struct->updJobs=AQFINTS_UpdJob_List_dup(p_src->updJobs);
  }


  /* member "genericExtension" */
  if (p_struct->genericExtension) {
    free(p_struct->genericExtension); p_struct->genericExtension=NULL;
  }
  if (p_src->genericExtension) {
    p_struct->genericExtension=strdup(p_src->genericExtension);
  }


  /* member "runtimeFlags" */
    p_struct->runtimeFlags=p_src->runtimeFlags;


  return p_struct;
}

AQFINTS_ACCOUNTDATA *AQFINTS_AccountData_copy(AQFINTS_ACCOUNTDATA *p_struct, const AQFINTS_ACCOUNTDATA *p_src) {
  assert(p_struct);
  assert(p_src);
  /* member "accountNumber" */
  if (p_struct->accountNumber) {
    free(p_struct->accountNumber); p_struct->accountNumber=NULL;
  }
  if (p_src->accountNumber) {
    p_struct->accountNumber=strdup(p_src->accountNumber);
  }


  /* member "accountSuffix" */
  if (p_struct->accountSuffix) {
    free(p_struct->accountSuffix); p_struct->accountSuffix=NULL;
  }
  if (p_src->accountSuffix) {
    p_struct->accountSuffix=strdup(p_src->accountSuffix);
  }


  /* member "bankCode" */
  if (p_struct->bankCode) {
    free(p_struct->bankCode); p_struct->bankCode=NULL;
  }
  if (p_src->bankCode) {
    p_struct->bankCode=strdup(p_src->bankCode);
  }


  /* member "country" */
    p_struct->country=p_src->country;


  /* member "iban" */
  if (p_struct->iban) {
    free(p_struct->iban); p_struct->iban=NULL;
  }
  if (p_src->iban) {
    p_struct->iban=strdup(p_src->iban);
  }


  /* member "accountName" */
  if (p_struct->accountName) {
    free(p_struct->accountName); p_struct->accountName=NULL;
  }
  if (p_src->accountName) {
    p_struct->accountName=strdup(p_src->accountName);
  }


  /* member "customerId" */
  if (p_struct->customerId) {
    free(p_struct->customerId); p_struct->customerId=NULL;
  }
  if (p_src->customerId) {
    p_struct->customerId=strdup(p_src->customerId);
  }


  /* member "accountType" */
    p_struct->accountType=p_src->accountType;


  /* member "currency" */
  if (p_struct->currency) {
    free(p_struct->currency); p_struct->currency=NULL;
  }
  if (p_src->currency) {
    p_struct->currency=strdup(p_src->currency);
  }


  /* member "name1" */
  if (p_struct->name1) {
    free(p_struct->name1); p_struct->name1=NULL;
  }
  if (p_src->name1) {
    p_struct->name1=strdup(p_src->name1);
  }


  /* member "name2" */
  if (p_struct->name2) {
    free(p_struct->name2); p_struct->name2=NULL;
  }
  if (p_src->name2) {
    p_struct->name2=strdup(p_src->name2);
  }


  /* member "limitType" */
    p_struct->limitType=p_src->limitType;


  /* member "limitValue" */
  if (p_struct->limitValue) {
    AB_Value_free(p_struct->limitValue);
  }
  if (p_src->limitValue) {
    p_struct->limitValue=AB_Value_dup(p_src->limitValue);
  }


  /* member "limitDays" */
    p_struct->limitDays=p_src->limitDays;


  /* member "updJobs" */
  if (p_struct->updJobs) {
    AQFINTS_UpdJob_List_free(p_struct->updJobs); p_struct->updJobs=NULL;
  }
  if (p_src->updJobs) {
    p_struct->updJobs=AQFINTS_UpdJob_List_dup(p_src->updJobs);
  }


  /* member "genericExtension" */
  if (p_struct->genericExtension) {
    free(p_struct->genericExtension); p_struct->genericExtension=NULL;
  }
  if (p_src->genericExtension) {
    p_struct->genericExtension=strdup(p_src->genericExtension);
  }


  /* member "runtimeFlags" */
    p_struct->runtimeFlags=p_src->runtimeFlags;


  return p_struct;
}

const char *AQFINTS_AccountData_GetAccountNumber(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->accountNumber;
}

const char *AQFINTS_AccountData_GetAccountSuffix(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->accountSuffix;
}

const char *AQFINTS_AccountData_GetBankCode(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->bankCode;
}

int AQFINTS_AccountData_GetCountry(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->country;
}

const char *AQFINTS_AccountData_GetIban(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->iban;
}

const char *AQFINTS_AccountData_GetAccountName(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->accountName;
}

const char *AQFINTS_AccountData_GetCustomerId(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->customerId;
}

int AQFINTS_AccountData_GetAccountType(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->accountType;
}

const char *AQFINTS_AccountData_GetCurrency(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->currency;
}

const char *AQFINTS_AccountData_GetName1(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->name1;
}

const char *AQFINTS_AccountData_GetName2(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->name2;
}

int AQFINTS_AccountData_GetLimitType(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->limitType;
}

const AB_VALUE *AQFINTS_AccountData_GetLimitValue(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->limitValue;
}

int AQFINTS_AccountData_GetLimitDays(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->limitDays;
}

AQFINTS_UPDJOB_LIST *AQFINTS_AccountData_GetUpdJobs(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->updJobs;
}

const char *AQFINTS_AccountData_GetGenericExtension(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->genericExtension;
}

uint32_t AQFINTS_AccountData_GetRuntimeFlags(const AQFINTS_ACCOUNTDATA *p_struct){
  assert(p_struct);
  return p_struct->runtimeFlags;
}

void AQFINTS_AccountData_SetAccountNumber(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->accountNumber) {
    free(p_struct->accountNumber); p_struct->accountNumber=NULL;
  }
  if (p_src) {
    p_struct->accountNumber=strdup(p_src);
  }
  else {
    p_struct->accountNumber=NULL;
  }

}

void AQFINTS_AccountData_SetAccountSuffix(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->accountSuffix) {
    free(p_struct->accountSuffix); p_struct->accountSuffix=NULL;
  }
  if (p_src) {
    p_struct->accountSuffix=strdup(p_src);
  }
  else {
    p_struct->accountSuffix=NULL;
  }

}

void AQFINTS_AccountData_SetBankCode(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->bankCode) {
    free(p_struct->bankCode); p_struct->bankCode=NULL;
  }
  if (p_src) {
    p_struct->bankCode=strdup(p_src);
  }
  else {
    p_struct->bankCode=NULL;
  }

}

void AQFINTS_AccountData_SetCountry(AQFINTS_ACCOUNTDATA *p_struct, int p_src){
  assert(p_struct);
  p_struct->country=p_src;
}

void AQFINTS_AccountData_SetIban(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->iban) {
    free(p_struct->iban); p_struct->iban=NULL;
  }
  if (p_src) {
    p_struct->iban=strdup(p_src);
  }
  else {
    p_struct->iban=NULL;
  }

}

void AQFINTS_AccountData_SetAccountName(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->accountName) {
    free(p_struct->accountName); p_struct->accountName=NULL;
  }
  if (p_src) {
    p_struct->accountName=strdup(p_src);
  }
  else {
    p_struct->accountName=NULL;
  }

}

void AQFINTS_AccountData_SetCustomerId(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->customerId) {
    free(p_struct->customerId); p_struct->customerId=NULL;
  }
  if (p_src) {
    p_struct->customerId=strdup(p_src);
  }
  else {
    p_struct->customerId=NULL;
  }

}

void AQFINTS_AccountData_SetAccountType(AQFINTS_ACCOUNTDATA *p_struct, int p_src){
  assert(p_struct);
  p_struct->accountType=p_src;
}

void AQFINTS_AccountData_SetCurrency(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->currency) {
    free(p_struct->currency); p_struct->currency=NULL;
  }
  if (p_src) {
    p_struct->currency=strdup(p_src);
  }
  else {
    p_struct->currency=NULL;
  }

}

void AQFINTS_AccountData_SetName1(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->name1) {
    free(p_struct->name1); p_struct->name1=NULL;
  }
  if (p_src) {
    p_struct->name1=strdup(p_src);
  }
  else {
    p_struct->name1=NULL;
  }

}

void AQFINTS_AccountData_SetName2(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->name2) {
    free(p_struct->name2); p_struct->name2=NULL;
  }
  if (p_src) {
    p_struct->name2=strdup(p_src);
  }
  else {
    p_struct->name2=NULL;
  }

}

void AQFINTS_AccountData_SetLimitType(AQFINTS_ACCOUNTDATA *p_struct, int p_src){
  assert(p_struct);
  p_struct->limitType=p_src;
}

void AQFINTS_AccountData_SetLimitValue(AQFINTS_ACCOUNTDATA *p_struct, const AB_VALUE *p_src){
  assert(p_struct);
  if (p_struct->limitValue) {
    AB_Value_free(p_struct->limitValue);
  }
  if (p_src) {
    p_struct->limitValue=AB_Value_dup(p_src);
  }
  else {
    p_struct->limitValue=NULL;
  }

}

void AQFINTS_AccountData_SetLimitDays(AQFINTS_ACCOUNTDATA *p_struct, int p_src){
  assert(p_struct);
  p_struct->limitDays=p_src;
}

void AQFINTS_AccountData_SetUpdJobs(AQFINTS_ACCOUNTDATA *p_struct, AQFINTS_UPDJOB_LIST *p_src){
  assert(p_struct);
  if (p_struct->updJobs) {
    AQFINTS_UpdJob_List_free(p_struct->updJobs); p_struct->updJobs=NULL;
  }
  p_struct->updJobs=p_src;
}

void AQFINTS_AccountData_SetGenericExtension(AQFINTS_ACCOUNTDATA *p_struct, const char *p_src){
  assert(p_struct);
  if (p_struct->genericExtension) {
    free(p_struct->genericExtension); p_struct->genericExtension=NULL;
  }
  if (p_src) {
    p_struct->genericExtension=strdup(p_src);
  }
  else {
    p_struct->genericExtension=NULL;
  }

}

void AQFINTS_AccountData_SetRuntimeFlags(AQFINTS_ACCOUNTDATA *p_struct, uint32_t p_src){
  assert(p_struct);
  p_struct->runtimeFlags=p_src;
}

void AQFINTS_AccountData_AddRuntimeFlags(AQFINTS_ACCOUNTDATA *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->runtimeFlags|=p_src;
}


void AQFINTS_AccountData_SubRuntimeFlags(AQFINTS_ACCOUNTDATA *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->runtimeFlags&=~p_src;
}



AQFINTS_ACCOUNTDATA_LIST *AQFINTS_AccountData_List_dup(const AQFINTS_ACCOUNTDATA_LIST *p_src) {
  AQFINTS_ACCOUNTDATA_LIST *p_dest;
  AQFINTS_ACCOUNTDATA *p_elem;

  assert(p_src);
  p_dest=AQFINTS_AccountData_List_new();
  p_elem=AQFINTS_AccountData_List_First(p_src);
  while(p_elem) {
    AQFINTS_ACCOUNTDATA *p_cpy;

    p_cpy=AQFINTS_AccountData_dup(p_elem);
    AQFINTS_AccountData_List_Add(p_cpy, p_dest);
    p_elem=AQFINTS_AccountData_List_Next(p_elem);
  }

  return p_dest;
}

void AQFINTS_AccountData_ReadDb(AQFINTS_ACCOUNTDATA *p_struct, GWEN_DB_NODE *p_db){
  assert(p_struct);
  /* member "accountNumber" */
  if (p_struct->accountNumber) {
    free(p_struct->accountNumber); p_struct->accountNumber=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "accountNumber", 0, NULL); if (s) p_struct->accountNumber=strdup(s); }
  /* member "accountSuffix" */
  if (p_struct->accountSuffix) {
    free(p_struct->accountSuffix); p_struct->accountSuffix=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "accountSuffix", 0, NULL); if (s) p_struct->accountSuffix=strdup(s); }
  /* member "bankCode" */
  if (p_struct->bankCode) {
    free(p_struct->bankCode); p_struct->bankCode=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "bankCode", 0, NULL); if (s) p_struct->bankCode=strdup(s); }
  /* member "country" */
  p_struct->country=GWEN_DB_GetIntValue(p_db, "country", 0, 0);
  /* member "iban" */
  if (p_struct->iban) {
    free(p_struct->iban); p_struct->iban=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "iban", 0, NULL); if (s) p_struct->iban=strdup(s); }
  /* member "accountName" */
  if (p_struct->accountName) {
    free(p_struct->accountName); p_struct->accountName=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "accountName", 0, NULL); if (s) p_struct->accountName=strdup(s); }
  /* member "customerId" */
  if (p_struct->customerId) {
    free(p_struct->customerId); p_struct->customerId=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "customerId", 0, NULL); if (s) p_struct->customerId=strdup(s); }
  /* member "accountType" */
  p_struct->accountType=GWEN_DB_GetIntValue(p_db, "accountType", 0, 0);
  /* member "currency" */
  if (p_struct->currency) {
    free(p_struct->currency); p_struct->currency=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "currency", 0, NULL); if (s) p_struct->currency=strdup(s); }
  /* member "name1" */
  if (p_struct->name1) {
    free(p_struct->name1); p_struct->name1=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "name1", 0, NULL); if (s) p_struct->name1=strdup(s); }
  /* member "name2" */
  if (p_struct->name2) {
    free(p_struct->name2); p_struct->name2=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "name2", 0, NULL); if (s) p_struct->name2=strdup(s); }
  /* member "limitType" */
  p_struct->limitType=GWEN_DB_GetIntValue(p_db, "limitType", 0, 0);
  /* member "limitValue" */
  if (p_struct->limitValue) {
    AB_Value_free(p_struct->limitValue);
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "limitValue", 0, NULL); if (s) p_struct->limitValue=AB_Value_fromString(s); else p_struct->limitValue=NULL; }
  /* member "limitDays" */
  p_struct->limitDays=GWEN_DB_GetIntValue(p_db, "limitDays", 0, 0);
  /* member "updJobs" */
  if (p_struct->updJobs) {
    AQFINTS_UpdJob_List_free(p_struct->updJobs); p_struct->updJobs=NULL;
  }
  { GWEN_DB_NODE *dbSource; dbSource=GWEN_DB_GetGroup(p_db, GWEN_PATH_FLAGS_NAMEMUSTEXIST, "updJobs"); if (dbSource) { AQFINTS_UPDJOB_LIST *t; GWEN_DB_NODE *dbT; t=AQFINTS_UpdJob_List_new(); dbT=GWEN_DB_FindFirstGroup(dbSource, "element"); while(dbT) { AQFINTS_UPDJOB *elem; elem=AQFINTS_UpdJob_fromDb(dbT); if (elem) AQFINTS_UpdJob_List_Add(elem, t); dbT=GWEN_DB_FindNextGroup(dbT, "element"); } p_struct->updJobs=t; } else p_struct->updJobs=NULL; }
  /* member "genericExtension" */
  if (p_struct->genericExtension) {
    free(p_struct->genericExtension); p_struct->genericExtension=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "genericExtension", 0, NULL); if (s) p_struct->genericExtension=strdup(s); }
  /* member "runtimeFlags" */
  /* member "runtimeFlags" is volatile, just presetting it */
  p_struct->runtimeFlags=0;
}

int AQFINTS_AccountData_WriteDb(const AQFINTS_ACCOUNTDATA *p_struct, GWEN_DB_NODE *p_db) {
  int p_rv;

  assert(p_struct);
  /* member "accountNumber" */
  if (p_struct->accountNumber) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "accountNumber", p_struct->accountNumber); else { GWEN_DB_DeleteVar(p_db, "accountNumber"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "accountSuffix" */
  if (p_struct->accountSuffix) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "accountSuffix", p_struct->accountSuffix); else { GWEN_DB_DeleteVar(p_db, "accountSuffix"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "bankCode" */
  if (p_struct->bankCode) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "bankCode", p_struct->bankCode); else { GWEN_DB_DeleteVar(p_db, "bankCode"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "country" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "country", p_struct->country);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "iban" */
  if (p_struct->iban) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "iban", p_struct->iban); else { GWEN_DB_DeleteVar(p_db, "iban"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "accountName" */
  if (p_struct->accountName) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "accountName", p_struct->accountName); else { GWEN_DB_DeleteVar(p_db, "accountName"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "customerId" */
  if (p_struct->customerId) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "customerId", p_struct->customerId); else { GWEN_DB_DeleteVar(p_db, "customerId"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "accountType" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "accountType", p_struct->accountType);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "currency" */
  if (p_struct->currency) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "currency", p_struct->currency); else { GWEN_DB_DeleteVar(p_db, "currency"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "name1" */
  if (p_struct->name1) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "name1", p_struct->name1); else { GWEN_DB_DeleteVar(p_db, "name1"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "name2" */
  if (p_struct->name2) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "name2", p_struct->name2); else { GWEN_DB_DeleteVar(p_db, "name2"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "limitType" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "limitType", p_struct->limitType);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "limitValue" */
  if (p_struct->limitValue) { GWEN_BUFFER *tbuf=GWEN_Buffer_new(0, 64, 0, 1); AB_Value_toString(p_struct->limitValue, tbuf); p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "limitValue", GWEN_Buffer_GetStart(tbuf)); GWEN_Buffer_free(tbuf); } else { GWEN_DB_DeleteVar(p_db, "limitValue"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "limitDays" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "limitDays", p_struct->limitDays);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "updJobs" */
  { if (p_struct->updJobs) { GWEN_DB_NODE *dbDest; AQFINTS_UPDJOB *elem; dbDest=GWEN_DB_GetGroup(p_db, GWEN_DB_FLAGS_OVERWRITE_GROUPS, "updJobs"); assert(dbDest); p_rv=0; elem=AQFINTS_UpdJob_List_First(p_struct->updJobs); while(elem) { GWEN_DB_NODE *dbElem; dbElem=GWEN_DB_GetGroup(dbDest, GWEN_PATH_FLAGS_CREATE_GROUP, "element"); assert(dbElem); p_rv=AQFINTS_UpdJob_toDb(elem, dbElem); if (p_rv<0) { DBG_INFO(GWEN_LOGDOMAIN, "here (%d)", p_rv); break; } elem=AQFINTS_UpdJob_List_Next(elem); } } else p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "genericExtension" */
  if (p_struct->genericExtension) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "genericExtension", p_struct->genericExtension); else { GWEN_DB_DeleteVar(p_db, "genericExtension"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }


  /* member "runtimeFlags" is volatile, not writing to db */


  return 0;
}

AQFINTS_ACCOUNTDATA *AQFINTS_AccountData_fromDb(GWEN_DB_NODE *p_db) {
  AQFINTS_ACCOUNTDATA *p_struct;
  p_struct=AQFINTS_AccountData_new();
  AQFINTS_AccountData_ReadDb(p_struct, p_db);
  return p_struct;
}

int AQFINTS_AccountData_toDb(const AQFINTS_ACCOUNTDATA *p_struct, GWEN_DB_NODE *p_db) {
  return AQFINTS_AccountData_WriteDb(p_struct, p_db);
}

void AQFINTS_AccountData_AddUpdJob(AQFINTS_ACCOUNTDATA *st, AQFINTS_UPDJOB *j) { if (st->updJobs==NULL) st->updJobs=AQFINTS_UpdJob_List_new(); AQFINTS_UpdJob_List_Add(j, st->updJobs); }

/* code headers */

