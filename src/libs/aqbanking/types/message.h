/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "message.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifndef AB_MESSAGE_MESSAGE_H
#define AB_MESSAGE_MESSAGE_H


#ifdef __cplusplus
extern "C" {
#endif

/** @page P_AB_MESSAGE Structure AB_MESSAGE
<p>This page describes the properties of AB_MESSAGE.</p>



<h1>AB_MESSAGE</h1>



@anchor AB_MESSAGE_source
<h2>source</h2>

<p>Set this property with @ref AB_Message_SetSource(), get it with @ref AB_Message_GetSource().</p>


@anchor AB_MESSAGE_userId
<h2>userId</h2>

ID of the user in whose context the message has been received (see @ref AB_User_GetUniqueId).
<p>Set this property with @ref AB_Message_SetUserId(), get it with @ref AB_Message_GetUserId().</p>


@anchor AB_MESSAGE_accountId
<h2>accountId</h2>

ID of the account in whose context the message has been received (if any). This field is not set by AqHBCI but may be used by other backends in some cases.
<p>Set this property with @ref AB_Message_SetAccountId(), get it with @ref AB_Message_GetAccountId().</p>


@anchor AB_MESSAGE_subject
<h2>subject</h2>

<p>Set this property with @ref AB_Message_SetSubject(), get it with @ref AB_Message_GetSubject().</p>


@anchor AB_MESSAGE_text
<h2>text</h2>

<p>Set this property with @ref AB_Message_SetText(), get it with @ref AB_Message_GetText().</p>


@anchor AB_MESSAGE_dateReceived
<h2>dateReceived</h2>

<p>Set this property with @ref AB_Message_SetDateReceived(), get it with @ref AB_Message_GetDateReceived().</p>

*/

/* needed system headers */
#include <gwenhywfar/types.h>
#include <gwenhywfar/list1.h>
#include <gwenhywfar/list2.h>
#include <gwenhywfar/db.h>
#include <gwenhywfar/xml.h>

/* pre-headers */
#include <aqbanking/error.h>
#include <gwenhywfar/types.h>
#include <gwenhywfar/gwentime.h>

typedef struct AB_MESSAGE AB_MESSAGE;
GWEN_LIST_FUNCTION_LIB_DEFS(AB_MESSAGE, AB_Message, AQBANKING_API)
GWEN_LIST2_FUNCTION_LIB_DEFS(AB_MESSAGE, AB_Message, AQBANKING_API)



typedef enum {
  AB_Message_SourceUnknown = -1,
  AB_Message_SourceNone = 0,
  AB_Message_SourceSystem,
  AB_Message_SourceBank
} AB_MESSAGE_SOURCE;


/* post-headers */


AQBANKING_API AB_MESSAGE_SOURCE AB_Message_Source_fromString(const char *p_s);

AQBANKING_API const char *AB_Message_Source_toString(AB_MESSAGE_SOURCE p_i);

/** Constructor. */
AQBANKING_API AB_MESSAGE *AB_Message_new(void);

/** Destructor. */
AQBANKING_API void AB_Message_free(AB_MESSAGE *p_struct);

AQBANKING_API void AB_Message_Attach(AB_MESSAGE *p_struct);

AQBANKING_API AB_MESSAGE *AB_Message_dup(const AB_MESSAGE *p_src);

AQBANKING_API AB_MESSAGE *AB_Message_copy(AB_MESSAGE *p_struct, const AB_MESSAGE *p_src);

/** Getter.
 * Use this function to get the member "source" (see @ref AB_MESSAGE_source)
*/
AQBANKING_API AB_MESSAGE_SOURCE AB_Message_GetSource(const AB_MESSAGE *p_struct);

/** Getter.
 * Use this function to get the member "userId" (see @ref AB_MESSAGE_userId)
*/
AQBANKING_API uint32_t AB_Message_GetUserId(const AB_MESSAGE *p_struct);

/** Getter.
 * Use this function to get the member "accountId" (see @ref AB_MESSAGE_accountId)
*/
AQBANKING_API uint32_t AB_Message_GetAccountId(const AB_MESSAGE *p_struct);

/** Getter.
 * Use this function to get the member "subject" (see @ref AB_MESSAGE_subject)
*/
AQBANKING_API const char *AB_Message_GetSubject(const AB_MESSAGE *p_struct);

/** Getter.
 * Use this function to get the member "text" (see @ref AB_MESSAGE_text)
*/
AQBANKING_API const char *AB_Message_GetText(const AB_MESSAGE *p_struct);

/** Getter.
 * Use this function to get the member "dateReceived" (see @ref AB_MESSAGE_dateReceived)
*/
AQBANKING_API const GWEN_TIME *AB_Message_GetDateReceived(const AB_MESSAGE *p_struct);

/** Setter.
 * Use this function to set the member "source" (see @ref AB_MESSAGE_source)
*/
AQBANKING_API void AB_Message_SetSource(AB_MESSAGE *p_struct, AB_MESSAGE_SOURCE p_src);

/** Setter.
 * Use this function to set the member "userId" (see @ref AB_MESSAGE_userId)
*/
AQBANKING_API void AB_Message_SetUserId(AB_MESSAGE *p_struct, uint32_t p_src);

/** Setter.
 * Use this function to set the member "accountId" (see @ref AB_MESSAGE_accountId)
*/
AQBANKING_API void AB_Message_SetAccountId(AB_MESSAGE *p_struct, uint32_t p_src);

/** Setter.
 * Use this function to set the member "subject" (see @ref AB_MESSAGE_subject)
*/
AQBANKING_API void AB_Message_SetSubject(AB_MESSAGE *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "text" (see @ref AB_MESSAGE_text)
*/
AQBANKING_API void AB_Message_SetText(AB_MESSAGE *p_struct, const char *p_src);

/** Setter.
 * Use this function to set the member "dateReceived" (see @ref AB_MESSAGE_dateReceived)
*/
AQBANKING_API void AB_Message_SetDateReceived(AB_MESSAGE *p_struct, const GWEN_TIME *p_src);

AQBANKING_API AB_MESSAGE_LIST *AB_Message_List_dup(const AB_MESSAGE_LIST *p_src);

AQBANKING_API void AB_Message_ReadDb(AB_MESSAGE *p_struct, GWEN_DB_NODE *p_db);

AQBANKING_API int AB_Message_WriteDb(const AB_MESSAGE *p_struct, GWEN_DB_NODE *p_db);

AQBANKING_API AB_MESSAGE *AB_Message_fromDb(GWEN_DB_NODE *p_db);

AQBANKING_API int AB_Message_toDb(const AB_MESSAGE *p_struct, GWEN_DB_NODE *p_db);

AQBANKING_API void AB_Message_ReadXml(AB_MESSAGE *p_struct, GWEN_XMLNODE *p_db);

AQBANKING_API void AB_Message_WriteXml(const AB_MESSAGE *p_struct, GWEN_XMLNODE *p_db);

AQBANKING_API void AB_Message_toXml(const AB_MESSAGE *p_struct, GWEN_XMLNODE *p_db);

AQBANKING_API AB_MESSAGE *AB_Message_fromXml(GWEN_XMLNODE *p_db);

AQBANKING_API void AB_Message_toHashString(const AB_MESSAGE *p_struct, GWEN_BUFFER *p_buffer);

AQBANKING_API int AB_Message_List2_freeAll(AB_MESSAGE_LIST2 *tl);
/* end-headers */


#ifdef __cplusplus
}
#endif

#endif

