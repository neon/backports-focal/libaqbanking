------------------------------------------------------------------
2021-09-18 17:36:09 +0200 Martin Preuss
Prepared release 6.3.2.

------------------------------------------------------------------
2021-09-11 19:56:57 +0200 Martin Preuss
aqebics: Use aqbanking-specific macro "AQBANKING_DEPRECATED" instead of "DEPRECATED".

------------------------------------------------------------------
2021-09-11 19:55:58 +0200 Martin Preuss
aqhbci: Fns GWEN_Gui_ShowBox() and GWEN_Gui_HideBox() are deprecated, don't use them.

------------------------------------------------------------------
2021-09-10 16:25:13 +0200 Martin Preuss
Released 6.3.1

------------------------------------------------------------------
2021-09-10 14:17:52 +0200 Martin Preuss
Prepared release 6.3.1.

------------------------------------------------------------------
2021-07-14 18:56:58 +0200 Martin Preuss
Decreased verbosity.

------------------------------------------------------------------
2021-04-01 16:04:13 +0200 Tobias Deiminger
Send acknowledgements (HKQTG)
FinTS 3.0 defines an acknowledge workflow (C.9.4, "Empfangsquittung"):
> By sending this segment, the customer acknowledges they have
> correctly received a bank response. The acknowledgement always
> relates to the direclty preceding bank response.
> Optionally, the acknowledgement can be supplemented by a code
> ("Quittungscode", e.g. a hash value), which allows the bank to draw
> addtional conclusion about the job to be acknowledged.

In practice, this means you can mark documents in the inbox of a
banking web client as "read", and prevent your bank from sending
you printed documents by post.

FinTS defines acknwoledgement only for a subset of jobs
- HIEKA "Quittungscode"
  BPD HIEKAS "Parameter Kontoauszug":"Quittierung benoetig"
- HIECA "Quittungscode"
  BPD HIEKAS "Parameter Kontoauszug camt":"Quittierung benoetig"
- HIEKP "Quittungscode"
  BPD HIEKPS "Parameter Kontoauszug PDF":"Quittierung benoetig"
- HIKAA "Quittungscode"
  BPD HIKAA "Postfach Nachricht abrufen":"Quittierung (fallweise)
  benoetigt"

This commit adds some generic handling, and fully enables acknowledgment
for HIEKA and HIEKP, i.e. AB_Transaction_CommandGetEStatements.

Users can enable acknowledgement with a new CLI option for the request
command:
$ aqbanking-cli -P /tmp/pinfile request --estatements -a 1234567 -c est.ctx --fromdate=20200101 --docnumber=10 --acknowledge

or they enable it via API by
AB_Transaction_SetAcknowledge(myTransaction, AB_Transaction_AckJobsWithAckCode).

Disclaimer: I don't understand the spec in how "preceding response" is
supposed to be interpreted for multi-segment responses and if the optional
acknowledge code was not sent by the bank. Seems the spec leaves some doubt
there, it could mean
- ack all segments from the preceding multi-segment response
- ack only the first segment from the preceding multi-segment response
- ack only the last segement
- ... whatever, you get it

Therefore I'm only implementing cases where an acknowledgement code was
given, because it allows to definitely identify the segment we mean.

------------------------------------------------------------------
2021-06-06 19:01:53 +0200 Martin Preuss
aqbanking-cli: Fixed argument for document number
- minnum and maxnum specify the minimum and maximum *occurrence* of an
  argument, not the allowed range for values
- changed argument name to "--docnumber=xxx"

------------------------------------------------------------------
2021-06-06 18:53:40 +0200 Martin Preuss
aqbanking-cli: Fixed a bug.

------------------------------------------------------------------
2021-05-05 17:31:21 +0200 Martin Preuss
Prepared release 6.3.0

------------------------------------------------------------------
2021-05-05 16:37:57 +0200 Martin Preuss
Prepared release 6.3.0stable.

------------------------------------------------------------------
2021-05-04 21:49:10 +0200 Martin Preuss
aqfints: Made remaining include instructions more specific.

------------------------------------------------------------------
2021-05-04 20:26:02 +0200 Martin Preuss
aqfints: Fixed makefile.

------------------------------------------------------------------
2021-05-04 20:25:46 +0200 Martin Preuss
aqfints: Make include instructions more specific.

------------------------------------------------------------------
2021-05-04 20:24:30 +0200 Martin Preuss
aqhbci: Changed wording.

------------------------------------------------------------------
2021-05-04 20:24:07 +0200 Martin Preuss
aqhbci: Make BIC variable.
Should at least fix the problem of encoding errors immediately before sending
a message due to missing BIC.

------------------------------------------------------------------
2021-05-04 20:22:43 +0200 Martin Preuss
aqfints: Adjusted file content to be usable with gwenbuild.

------------------------------------------------------------------
2021-05-04 20:21:22 +0200 Martin Preuss
BUILD: Added 0BUILD files for aqfints.

------------------------------------------------------------------
2021-05-02 23:39:14 +0200 Martin Preuss
AqBanking: Only log job info for job id != 0.

------------------------------------------------------------------
2021-05-02 23:15:58 +0200 Martin Preuss
AqHBCI: Show errornous version number.

------------------------------------------------------------------
2021-05-02 22:59:57 +0200 Martin Preuss
AqHBCI: Fixed compiler warnings.

------------------------------------------------------------------
2021-05-02 22:38:57 +0200 Martin Preuss
AqBanking, AqHBCI: Use new job logging functions.

------------------------------------------------------------------
2021-05-02 22:38:27 +0200 Martin Preuss
AqHBCI: Increased verbosity.

------------------------------------------------------------------
2021-05-02 22:36:08 +0200 Martin Preuss
AqBanking: Added job logging functions to be used by backends.
- AB_Banking_LogMsgForJobId()
- AB_Banking_LogCmdInfoMsgForJob()

------------------------------------------------------------------
2021-05-02 22:34:54 +0200 Martin Preuss
BUILD: Added targets for xgettext and astyle.

------------------------------------------------------------------
2021-04-22 00:18:07 +0200 Martin Preuss
BUILD: Add needed libs when building static lib.

------------------------------------------------------------------
2021-04-20 22:05:50 +0200 Martin Preuss
build: Fixed gwenbuild files.

------------------------------------------------------------------
2021-04-20 21:42:45 +0200 Martin Preuss
build: More work on gwenbuild files.

------------------------------------------------------------------
2021-04-18 21:42:29 +0200 Martin Preuss
BUILD: Minor fixes to 0BUILD files.

------------------------------------------------------------------
2021-04-15 16:29:37 +0200 Martin Preuss
BUILD: Also build internal tool "mkdeinfo".

------------------------------------------------------------------
2021-04-15 16:19:09 +0200 Martin Preuss
BUILD: gwbuild now also builds tools.

------------------------------------------------------------------
2021-04-15 15:04:18 +0200 Martin Preuss
BUILD: More work on gwenbuild setup.

------------------------------------------------------------------
2021-04-14 23:04:27 +0200 Martin Preuss
BUILD: gwbuild now also compiles swift module.

------------------------------------------------------------------
2021-04-12 21:50:02 +0200 Martin Preuss
BUILD: Using xmlmerge now works across subfolders.

------------------------------------------------------------------
2021-04-11 18:40:43 +0200 Martin Preuss
aqbankingpp: Minor changes, hasn't been updated for a while.
Currently doesn't compile, remains deactivated in 0BUILD.

------------------------------------------------------------------
2021-04-11 17:43:38 +0200 Martin Preuss
BUILD: Use short version of <checkProgs>.

------------------------------------------------------------------
2021-04-11 17:43:15 +0200 Martin Preuss
BUILD: Start using xmlmerge to merge XML files also with gwbuild.

------------------------------------------------------------------
2021-04-11 01:49:03 +0200 Martin Preuss
BUILD: Define "AQBANKING_VERSION_TAG".

------------------------------------------------------------------
2021-04-11 01:35:23 +0200 Martin Preuss
BUILD: Fixed toplevel 0BUILD to emulate Makefile.am behaviour.

------------------------------------------------------------------
2021-04-11 01:17:06 +0200 Martin Preuss
BUILD: Improved "typefiles" and "typedefs" targets.
We need to build those targets at least for tanmethod.xml.

------------------------------------------------------------------
2021-04-11 01:18:30 +0200 Martin Preuss
AqOfxConnect: Include version.h.
This is in fact needed (need to adapt corresponding 0BUILD file as well).

------------------------------------------------------------------
2021-04-11 01:17:44 +0200 Martin Preuss
AqHBCI: Added .gitignore.

------------------------------------------------------------------
2021-04-11 01:07:50 +0200 Martin Preuss
Revert "AqBanking: Don't include "version.h". This is intended for outside code."
This reverts commit f6f1e965e6792f0ebe826b1b0be399ebd5984981.

------------------------------------------------------------------
2021-04-11 01:07:27 +0200 Martin Preuss
Added chapter about gwbuild.

------------------------------------------------------------------
2021-04-11 00:47:57 +0200 Martin Preuss
BUILD: Added initial support for gwbuild.
Not yet polished, also xmlmerge doesn't work, yet. But already compiles
and links.

------------------------------------------------------------------
2021-04-11 00:45:43 +0200 Martin Preuss
AqBanking: Add Makefile target to clean generated files.

------------------------------------------------------------------
2021-04-11 00:45:14 +0200 Martin Preuss
AqBanking: Don't include "version.h". This is intended for outside code.
The file doesn't contain anything AqBanking doesn't already know about.

------------------------------------------------------------------
2021-04-11 00:44:26 +0200 Martin Preuss
AqHBCI: Fixed makefile targets to use typemaker2 instead of typemaker1.

------------------------------------------------------------------
2021-04-11 00:42:35 +0200 Martin Preuss
AqBanking: Replaced "@" with " at " of email in source header.
Avoid problem with uneven number of "@" in rewritten file.

------------------------------------------------------------------
2021-04-11 00:40:15 +0200 Martin Preuss
AqHBCI: Ported tanmethod from typemaker to typemaker2.

------------------------------------------------------------------
2021-04-11 00:39:14 +0200 Martin Preuss
AqHBCI: Make #include statements more specific.
Will need less "-I" cflags in the long run.

------------------------------------------------------------------
2021-04-11 00:35:00 +0200 Martin Preuss
AqBanking: No longer include compile time.
It was once relevant to determine whether the sources were recompiled but
nowadays it produces more problems than it solves.

------------------------------------------------------------------
2021-04-02 21:18:14 +0200 Tobias Deiminger
Allow to select specific estatements by year/nr
For FinTS estatements (HKEKA, HKEKP), the classic from-date..to-date
timespan selection is not supported. Instead, if supported by the bank,
we can select estatements by year and by document number, and we can
further limit number of retrieved documents by setting max. entries.

This commit implements the described selection parameters. They're
exposed as new AB_Transaction members estatementNumber and
estatementMaxEntries, and by a new CLI argument "number". Omitting or
setting to 0 disables the parameters. Year is taken from the already
existing firstDate member (relates to "fromdate" CLI argument). Format
is YYYYMMDD, so MMDD is ignored.

Example usage:

aqbanking-cli request --estatements -a 1234567 -c est.ctx --fromdate=20200101 --number=3

This will download only the third estatement from the year 2020.

------------------------------------------------------------------
2021-03-20 15:27:04 +0100 Martin Preuss
AqHBCI: Added job HKQTG (acknowledge received document).
Not quite sure how to use that, though.

------------------------------------------------------------------
2021-03-20 14:17:27 +0100 Martin Preuss
aqbanking-cli: Corrected copyright notice.

------------------------------------------------------------------
2021-03-20 14:17:01 +0100 Martin Preuss
aqbanking-cli: Added command "getdoc".

------------------------------------------------------------------
2021-03-20 12:17:31 +0100 Martin Preuss
AqHBCI: Fixed error code.

------------------------------------------------------------------
2021-03-20 01:28:31 +0100 Martin Preuss
AqHBCI: Fix the way we look for an already existing file.

------------------------------------------------------------------
2021-03-20 01:23:49 +0100 Martin Preuss
AqHBCI: Added missing path flag.

------------------------------------------------------------------
2021-03-19 17:19:39 +0100 Martin Preuss
AqHBCI: Use shorter path for eStatements.

------------------------------------------------------------------
2021-03-19 16:58:31 +0100 Martin Preuss
aqbanking-cli: Allow "listdoc" to show path of documents.

------------------------------------------------------------------
2021-03-19 16:53:20 +0100 Martin Preuss
AqHBCI: Store received electronic statements in customer folder.
AB_DOCUMENT inside a ImExporterContext no longer contains the data itself
when added to an ImExporterContext, it now only contains the path of a file
which contains the documents data.

This allows for storage of even large data received from the bank.

------------------------------------------------------------------
2021-03-19 16:12:21 +0100 Martin Preuss
AqBanking: Add member "filePath" to AB_Document.
A document object now can either directly contain its data in the "data"
member or just contain the name of file where the data is stored.

------------------------------------------------------------------
2021-03-19 16:01:55 +0100 Martin Preuss
aqbanking-cli: Add function "listdoc" which lists the eStatement docs in a ctxfile.
Saving eStatements inside a ctxfile might not be a good idea because they
tend to become large (at least 50k each).

Storing such big values in a ctxfile works astonishingly well but opening
such a file in an editor (like FTE) takes quite a long time...

------------------------------------------------------------------
2021-03-18 23:42:16 +0100 Martin Preuss
AqHBCI: Added TODO.

------------------------------------------------------------------
2021-03-18 23:42:01 +0100 Martin Preuss
AqHBCI: Cleanup accountjob.*
- prepared removal of AH_BANK_FLAGS_KTV2
- remove unused functions

------------------------------------------------------------------
2021-03-18 22:55:08 +0100 Martin Preuss
AqHBCI: Cleaned up jobgettrans_camt.

------------------------------------------------------------------
2021-03-18 22:44:58 +0100 Martin Preuss
AqHBCI: Started removing unneeded remarks.
Those where used in the old days to easily find the start of functions.
However, nowadays the lines are sometimes longer than these remarks, so
they don't help anymore, anyway. Getting rid of them...

------------------------------------------------------------------
2021-03-18 21:08:35 +0100 Martin Preuss
AqHBCI: Simplified job GetEStatements.
GetEStatements and GetEStatements2 share the same code, so there is no need
for extra code.
The differences are handled by the XML files.

------------------------------------------------------------------
2021-03-18 20:57:53 +0100 Martin Preuss
AqHBCI: Assign id and mimetype to eStatement docs.

------------------------------------------------------------------
2021-03-18 20:57:19 +0100 Martin Preuss
COPYING: Added Tobias Deiminger as Contributor (Thanks!!)

------------------------------------------------------------------
2021-03-17 22:13:39 +0100 Martin Preuss
AqHBCI: Just a little reformatting.

------------------------------------------------------------------
2021-03-17 22:04:03 +0100 Martin Preuss
AqHBCI: Enable code to add received HKEKA eStatement to ImExporterContext.

------------------------------------------------------------------
2021-03-17 21:51:28 +0100 Martin Preuss
AqHBCI/AqBanking: Modified Tobias's patch to not add a job type to AqBanking.
We just use the same job type for both HBCI jobs and let the bank decide
which job to use (like it's already done with other HBCI jobs).

We now first check whether the bank supports "HKEKP", if it doesn't we
try "HKEKA".

------------------------------------------------------------------
2021-03-17 00:27:23 +0100 Tobias Deiminger
Add another get estatments variant (HKEKA)
For HBCI there are multiple ways for how to fetch electronic estatements.
AqBanking already implements HKEKP to download PDFs, but some banks don't
support HKEKP but rather support HKEKA which has also the option provide PDFs.

This commit adds the option to execute
 aqbanking-cli request --estatements2
which will send HBCI HKEKA requests and receive HIEKA responses, which
contain the binary estatements.

------------------------------------------------------------------
2021-03-15 21:26:28 +0100 Martin Preuss
AqHBCI: Clarified debug message.

------------------------------------------------------------------
2021-03-15 21:22:35 +0100 Martin Preuss
AqHBCI: Changed message (re-queuing really is used).

------------------------------------------------------------------
2021-03-10 21:16:56 +0100 Martin Preuss
Released 6.2.10

------------------------------------------------------------------
2021-03-10 20:45:30 +0100 Martin Preuss
Prepared release 6.2.10.

------------------------------------------------------------------
2021-03-10 19:55:47 +0100 Martin Preuss
AqHBCI: Increased verbosity for debugging purposes.

------------------------------------------------------------------
2021-03-10 19:55:28 +0100 Martin Preuss
AqHBCI: Fixed a bug regarding TAN-secured jobs.
- need to set the status of a sent TAN job accordingly, otherwise its
  status will not be updated when receiving responses. In that case the
  code assumes the TAN job has not been sent so there can't be any response
  for it, ergo it will be ignored when dispatching responses.
- the response to the 2nd TAN request in TAN process 2/4 is a response
  only for that particular TAN job, not for the jobs which are secured by
  the TAN job.
  Those secured jobs are sent in the first message along with a TAN offer.
  If the server decides it doesn't need a TAN it just sends direct
  responses for those jobs.
  However, if the server does request a TAN we only get a response for the
  TAN offer, not for the TAN-secured jobs. Actually, in this case we dont
  get responses for those jobs at all... In such a case we have to take
  the response for the 2nd TAN request and distribute them among the
  TAN-secured jobs.
  I wonder how that works for TAN-secured requests which expect a dedicated
  response (e.g. request for bank statements)... how will those be delivered
  in a complete different message??

------------------------------------------------------------------
2021-02-22 22:20:40 +0100 Martin Preuss
Released 6.2.9.

------------------------------------------------------------------
2021-02-22 22:19:46 +0100 Martin Preuss
Prepared release 6.2.9.

------------------------------------------------------------------
2021-02-20 16:42:43 +0100 Martin Preuss
AqHBCI: Remove trailing blanks in mimetype.

------------------------------------------------------------------
2021-02-20 00:05:02 +0100 Martin Preuss
AqBanking: Dont warn about our own use of deprecated functions.
AB_GUI uses some of those functions and will continue to do so, however
their definition will go into banking_be.h and no longer be exported for
AqBanking7. So those functions will stay inside AqBanking and still used
internally but no longer exported.

------------------------------------------------------------------
2021-02-20 00:03:39 +0100 Martin Preuss
AqBanking: Mark some internal functions as deprecated.
Only one of these functions is still used by Gnucash (AB_Banking_LoadSharedConfig)
to get the previously shared certificate cache,
and even that call doesn't do anything anymore since AqBanking stores the
certificates inside the user configuration for some time now (the shared
certificate cache isn't used anymore).

------------------------------------------------------------------
2021-02-20 00:00:39 +0100 Martin Preuss
Updated TODO.

------------------------------------------------------------------
2021-02-19 19:02:56 +0100 Martin Preuss
Updated README.

------------------------------------------------------------------
2021-02-19 18:42:25 +0100 Martin Preuss
Released 6.2.8

------------------------------------------------------------------
2021-02-19 18:42:05 +0100 Martin Preuss
Prepared release 6.2.8.

------------------------------------------------------------------
2021-02-19 14:26:44 +0100 Martin Preuss
AqHBCI: Increased verbosity for failed decryption.

------------------------------------------------------------------
2021-02-19 13:10:09 +0100 Martin Preuss
AqHBCI: Fixed a double-free.

------------------------------------------------------------------
2021-02-16 22:35:25 +0100 Christian Stimming
i18n: Update German translation
794t, 18f, 279u

------------------------------------------------------------------
2021-02-16 22:31:45 +0100 Christian Stimming
i18n: Mark some forgotten strings for translation

------------------------------------------------------------------
2021-02-16 22:14:20 +0100 Christian Stimming
i18n: Update German translation
793t, 18f, 279u

------------------------------------------------------------------
2021-02-16 22:13:47 +0100 Christian Stimming
i18n: Some minor string fixes in the code

------------------------------------------------------------------
2021-02-16 21:16:40 +0100 Christian Stimming
i18n: Add some forgotten files to i18nsources

------------------------------------------------------------------
2021-02-15 21:04:31 +0100 Martin Preuss
Released 6.2.7

------------------------------------------------------------------
2021-02-15 20:38:39 +0100 Martin Preuss
Prepared release 6.2.7.

------------------------------------------------------------------
2021-02-15 18:51:25 +0100 Martin Preuss
XML: Improved OFX import handling (needs latest gwen).
