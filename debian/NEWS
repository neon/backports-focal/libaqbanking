libaqbanking (5.99.43beta-2) unstable; urgency=medium

  Important notice for users upgrading from versions prior to 6.x
  ===============================================================

  With AqBanking 6.x (including the preceding 5.99.x beta versions) the file
  location where AqBanking applications store their online banking related
  settings got changed again.  Previously the AqBanking settings were stored in
  the directory ~/.aqbanking/settings/.  After the upgrade settings are now
  stored in the directory ~/.aqbanking/settings6/.  This means without manual
  interaction all the online banking account setup will be unavailable to the
  applications using it.

  Luckily a migration of the old settings to the new location is possible.
  Just copy all files from ~/.aqbanking/settings/ to ~/.aqbanking/settings6

      cd ~/.aqbanking
      mkdir -p settings6
      cp -r settings/* settings6

  This should make the old online banking account settings available to the new
  library version.

  Updating the TAN methods to use for inline banking
  --------------------------------------------------

  Due to legal changes (PSD2) most banks in Germany were forced to switch to a
  different TAN method for authorizing transactions.  When upgrading from a
  libaqbanking version prior to 6.x, outdated TAN methods might still be cached
  in the library settings.  Please follow these steps to update the TAN methods
  for your accounts after the upgrade:

      * in the online banking application open the settings dialog for AqBanking
        (varies, depending on the used application)
      * select the relevant user and click "Edit User"
      * in the dialog please click the "Get Bank Info" button (this will
        anonymously fetch the current bank and user parameter data from the
        bank's server)
      * in the dialog please click the "Get System Id" button (this will fetch
        a new system identifier and the list of allowed TAN modes from the
        bank's server)
      * after success, select a TAN method attributed with "Version 6" or higher
      * maybe close the dialog for now to open the "Edit User" dialog anew
      * in the dialog click the "Retrieve Account List" button (this will fetch
        the current account list from the bank's server, from now on TAN in
        version 6 will be used).

  In case you struggle with this update process, consider subscribing to the
  https://mailman.aqbanking.de/listinfo/aqbanking-user mailing list and ask for
  help (German and English language welcome).

 -- Micha Lenk <micha@debian.org>  Tue, 05 Nov 2019 21:15:06 +0100

libaqbanking (3.0.1-3) unstable; urgency=low

  Important information for users upgrading from versions prior to 3.x
  ====================================================================

  * Directory for AqBanking's configuration and data storage changed
    ----------------------------------------------------------------

    Users which were using prior versions of AqBanking should know that
    AqBanking now stores its configuration and data in a different directory
    than previous versions did. There are very good chances that you can reuse
    your old configuration, but you need to copy all old files into a new
    directory. For that purpose execute the following command in order to take
    over your old configuration for use with AqBanking 3:

       cp -a ~/.banking ~/.aqbanking

  * Key file format changed
    -----------------------

    Unfortunately the format of very old AqBanking (former OpenHBCI) key files
    is not supported with AqBanking 3.x any more. Thus, if you are using a key
    file, you possibly need to update it, especially if you created it using
    OpenHBCI (the ancestor of AqBanking). For further information on how to
    update your key file please read the instructions in file

      /usr/share/doc/aqbanking-tools/README.keyfile-update

  If other users using your system might be affected please inform them about
  the changes mentioned above.

 -- Micha Lenk <micha@lenk.info>  Sun, 30 Dec 2007 16:31:58 +0100
