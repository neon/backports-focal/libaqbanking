#!/usr/bin/make -f

%:
	dh $@

# Let dpkg-gensymbols fail if symbols file updates are needed
DPKG_GENSYMBOLS_CHECK_LEVEL ?= 4
export DPKG_GENSYMBOLS_CHECK_LEVEL

# Support reproducible builds by force generated timestamps to in UTC.
# This is used by the command 'date' called within ./configure
TZ=UTC
export TZ

include /usr/share/dpkg/architecture.mk

override_dh_auto_configure:
	dh_auto_configure --max-parallel=1 -- \
		--with-build-datetime="$(shell dpkg-parsechangelog -c1 -S date)" \
		--with-backends="aqnone aqhbci aqofxconnect aqebics aqpaypal"

override_dh_auto_test:
	# Skip dh_auto_test (does not work with AqBanking 3.7.2)

override_dh_auto_build:
	dh_auto_build --max-parallel=1
	$(MAKE) -C debian/doc LANG=C

override_dh_auto_install:
	dh_auto_install
	# Remove .la files for shared plugin libraries
	find $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/aqbanking/plugins -name \*.la -exec rm -v \{\} \;

override_dh_makeshlibs:
	dh_makeshlibs -a -X/usr/lib/$(DEB_HOST_MULTIARCH)/aqbanking/plugins/

override_dh_auto_clean:
	dh_auto_clean --max-parallel=1
	@if [ -e $(CURDIR)/src/plugins/backends/aqyellownet ]; then \
	  echo "ERROR: The directory src/plugins/backends/aqyellownet exists. This usually"; \
	  echo "indicates that you're using a pristine upstream tarball which distributes"; \
	  echo "non-free material in this directory. This is unacceptable for Debian."; \
	  echo "Please see http://bugs.debian.org/469131 for further details."; \
	  exit 1; \
	fi
	$(MAKE) -C debian/doc clean
