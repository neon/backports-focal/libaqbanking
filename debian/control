Source: libaqbanking
Section: libs
Priority: optional
Maintainer: Micha Lenk <micha@debian.org>
Build-Depends: debhelper-compat (= 13), pkg-config,
  libgwenhywfar-core-dev (>= 5.6.0), gwenhywfar-tools,
  libktoblzcheck1-dev,
  libxmlsec1-dev,
  zlib1g-dev,
  doxygen,
  bzip2,
  libgmp10-dev,
  asciidoctor
Standards-Version: 4.6.0
Homepage: http://www.aquamaniac.de/aqbanking/
Vcs-Browser: https://salsa.debian.org/aqbanking-team/pkg-libaqbanking
Vcs-Git: https://salsa.debian.org/aqbanking-team/pkg-libaqbanking.git

Package: libaqbanking-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libaqbanking44 (= ${binary:Version}),
  libgwenhywfar-core-dev,
  libktoblzcheck1-dev,
  ${misc:Depends},
  ${perl:Depends}
Description: library for online banking applications
 AqBanking provides a middle layer between the applications
 and online banking libraries implementing various file formats and protocols.
 Plugins supporting OFX, DTAUS, HBCI, and EBICS are available.
 .
 This package contains the development files for AqBanking.

Package: aqbanking-tools
Section: utils
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: gwenhywfar-tools
Description: basic command line homebanking utilities
 AqBanking provides a middle layer between the applications
 and online banking libraries implementing various file formats and protocols.
 Plugins supporting OFX, DTAUS, HBCI, and EBICS are available.
 .
 This package provides a basic command line interface to AqBanking.

Package: libaqbanking-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: configuration files for libaqbanking
 AqBanking provides a middle layer between the applications
 and online banking libraries implementing various file formats and protocols.
 Plugins supporting OFX, DTAUS, HBCI, and EBICS are available.
 .
 This package contains data files for AqBanking.

Package: libaqbanking44
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
  libaqbanking-data (>= ${source:Version}),
  ${misc:Depends}
Suggests: aqbanking-tools
Description: library for online banking applications
 AqBanking provides a middle layer between the applications
 and online banking libraries implementing various file formats and protocols.
 Plugins supporting OFX, DTAUS, HBCI, and EBICS are available.
